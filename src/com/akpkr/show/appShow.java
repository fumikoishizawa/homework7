package com.akpkr.show;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class appShow extends HttpServlet {
	String[] urlList = new String[30];
	int countURL=0;
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		
		Enumeration names = req.getParameterNames();
		String parameter = (String)names.nextElement();
		String value = req.getParameter(parameter);
		
		resp.setContentType("text/plain");
		
		String url=Show("http://step-test-krispop.appspot.com/peers", 1);
			
		for(int i=0; i<countURL; i++){		
			resp.getWriter().println(urlList[i]);
			resp.getWriter().println(Show(urlList[i]+"/convert?message="+parameter, 0));
			resp.getWriter().println(" ");
		}
		
	
		//（ゝω・）vｷｬﾋﾟ
	}
	
	
	//URLの文字列を取得
	//flagは１の時、URL一覧を取得して、0の時、別サーバの値を取得する
	public String Show(String text, int flag){
      String lineTmp=null; String line=null;
      
      try {
    	    URL url = new URL(text);
    	    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

    	   
    	    while ((lineTmp = reader.readLine()) != null) {
    	    	if(flag==1) urlList[countURL++]=lineTmp;
    	    	if(flag==0) line=lineTmp;
    	    }
    	    reader.close();

    	} catch (MalformedURLException e) {
    	    // ...
    	} catch (IOException e) {
    	    // ...
    	}
      
      return line;
	}
	
	
}
