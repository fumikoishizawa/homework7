package com.akpkr.convert;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class WebappforSTEPServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		Enumeration names = req.getParameterNames();
		//パラメータ
		String parameter = (String)names.nextElement();
		String value = req.getParameter(parameter);
		
		resp.setContentType("text/plain");
		value=value+"( ˘ω˘)";
		resp.getWriter().println(value);
	
		
	}
	
	
}
